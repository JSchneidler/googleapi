
<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
	<title>TrailFinder</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" charset="utf-8">
	<base href="/">

	<!-- Bower Styles -->
	<link rel="stylesheet" type="text/css" href="components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="components/font-awesome/css/font-awesome.min.css">

	<!-- Custom CSS -->
	<link rel="stylesheet" type="text/css" href="angular/css/custom.css">

	<!-- Bower Components -->
	<script type="text/javascript" src="components/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript" src="components/angular/angular.min.js"></script>
	<script type="text/javascript" src="components/angular-route/angular-route.min.js"></script>
	<script type="text/javascript" src="components/bootstrap/dist/js/bootstrap.min.js"></script>

	<!-- Angular App -->
	<script type="text/javascript" src="angular/js/app.js"></script>
	<script type="text/javascript" src="angular/js/services.js"></script>
	<script type="text/javascript" src="angular/js/controllers.js"></script>

	<!-- Google API Service: async and defer prevent initGoogle() being initialized until API is loaded -->
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjhpde3UtPz_yoligWRJuhQ1VI0vy67lM&libraries=places&v=3" async defer></script>
</head>

<body ng-controller="MainController">

	<div ng-view></div>

	<img id="google" class="img-responsive" src="angular/img/google.png">
	
</body>
</html>