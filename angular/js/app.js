var app = angular.module("app", [
	"ngRoute",
	"Controllers"
	//"Services"
]);

app.config(["$routeProvider", "$locationProvider",
	function($routeProvider, $locationProvider) {
		$routeProvider
			.when("/", {
				templateUrl: "angular/partials/index.html",
				controller: "IndexController"
			})
			.when("/location/:placeId", {
				templateUrl: "angular/partials/location.html",
				controller: "LocationController"
			});

		$locationProvider.html5Mode(true);
	}
]);