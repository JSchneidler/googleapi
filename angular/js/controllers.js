
var Controllers = new angular.module("Controllers", []);

Controllers.controller("MainController", ["$scope", function($scope) {

	$(document).ready(function() {
		setInterval(function() {
			$("#google").css("bottom", 0, "left", 0);
		}, 25);
	});

}]);

Controllers.controller("IndexController", ["$scope", "$http", "$location", function($scope, $http, $location) {

	$scope.searchTerm = "";
	$scope.processing = false;

	var Autocomplete;
	$(document).ready(function() {

		Autocomplete = new google.maps.places.Autocomplete(document.getElementById("locationInput"), {});

		// Vertically center div on page
		setInterval(function() {
			$("#centered").css({"margin-top": ($(window).height()/2)-($("#centered").height()/2) });
		}, 25);

	});

	$scope.submit = function() {
		$scope.processing = true;
		$scope.location = Autocomplete.getPlace();
		if ($scope.location) {
			return $location.path("/location/" + $scope.location.place_id);
		}
		$scope.processing = false;
		return console.log("ERROR: Null location");
	}

}]);

Controllers.controller("LocationController", ["$routeParams", "$scope", "$location", function($routeParams, $scope, $location) {

	if (!$routeParams.placeId) return $location.path("/");

	var PlaceService;
	$(document).ready(function() {
		PlaceService = new google.maps.places.PlacesService(document.getElementById("locationContainer"));
	});

	PlaceService.getDetails({ placeId: $routeParams.placeId }, function(place, status) {
		PlaceService.nearbySearch({ location: place.geometry.location, radius: 99999 }, function(places, status) {
			if (status == google.maps.places.PlacesServiceStatus.OK) {
				$scope.$apply(function(){
					console.log(places);
	    		return $scope.locations = places;
				});
			} else {
				return console.log("ERROR: " + status);
			}
		});	
	});

}]);